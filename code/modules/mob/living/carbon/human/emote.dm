/mob/living/carbon/human/emote(var/act,var/m_type=1,var/message = null)
	var/param = null

	if (findtext(act, "-", 1, null))
		var/t1 = findtext(act, "-", 1, null)
		param = copytext(act, t1 + 1, length(act) + 1)
		act = copytext(act, 1, t1)

	if(findtext(act,"s",-1) && !findtext(act,"_",-2))//Removes ending s's unless they are prefixed with a '_'
		act = copytext(act,1,length(act))

	var/muzzled = istype(src.wear_mask, /obj/item/clothing/mask/muzzle)
	//var/m_type = 1

	for (var/obj/item/weapon/implant/I in src)
		if (I.implanted)
			I.trigger(act, src)

	if(src.stat == 2.0 && (act != "deathgasp"))
		return

	if(act == "oath" && src.miming)
		src.miming = 0
		for(var/obj/effect/proc_holder/spell/aoe_turf/conjure/mime_wall/s in src.spell_list)
			del(s)
		message_admins("� [src.name] ([src.ckey]) has broken their oath of silence. (<A HREF='?_src_=holder;adminplayerobservejump=\ref[src]'>JMP</a>)")
		src << "\red An unsettling feeling surrounds you..."
		return
	switch(act)
		if ("airguitar")
			if (!src.restrained())
				message = "� <B>[src]</B> ������� �� ������� � ������� �������, ��� ����� �����[LLZ]��." // Translate it 2916
				m_type = 1

		if ("blink")
			message = "� <B>[src]</B> �������." // Translate it 2917
			m_type = 1

		if ("blink_r")
			message = "� <B>[src]</B> ������ �������." // Translate it 2918
			m_type = 1

		if ("bow")
			if (!src.buckled)
				var/M = null
				if (param)
					for (var/mob/A in view(null, null))
						if (param == A.name)
							M = A
							break
				if (!M)
					param = null

				if (param)
					message = "� <B>[src]</B> ����[LLZ]���[LLZ] [param]." // Translate it 2919
				else
					message = "� <B>[src]</B> ����[LLZ]���[LLZ]." // Translate it 2920
			m_type = 1

		if ("custom")
			var/input = copytext(sanitize(input("Choose an emote to display.") as text|null),1,MAX_MESSAGE_LEN) // Translate it 2921
			if (!input)
				return
			var/input2 = input("��� ������[LLZ] ��� �������[LLZ] �����[LLZ]?") in list("������[LLZ]","�������[LLZ]") // Translate it 2921
			if (input2 == "������[LLZ]")
				m_type = 1
			else if (input2 == "�������[LLZ]")
				if (src.miming)
					return
				m_type = 2
			else
				alert("���������� ������������ ��� ������, ��� ������ ���� ������[LLZ] ��� �������[LLZ].") // Translate it 2922
				return
			return custom_emote(m_type, message)

		if ("me")
			if(silent)
				return
			if (src.client)
				if (client.prefs.muted & MUTE_IC)
					src << "\red You cannot send IC messages (muted)." // Translate it 2923
					return
				if (src.client.handle_spam_prevention(message,MUTE_IC))
					return
			if (stat)
				return
			if(!(message))
				return
			return custom_emote(m_type, message)

		if ("salute")
			if (!src.buckled)
				var/M = null
				if (param)
					for (var/mob/A in view(null, null))
						if (param == A.name)
							M = A
							break
				if (!M)
					param = null

				if (param)
					message = "� <B>[src]</B> ������ ����� [param]." // Translate it 2924
				else
					message = "� <B>[src]</b> ��������." // Translate it 2925
			m_type = 1

		if ("choke")
			if(miming)
				message = "� <B>[src]</B> ����[LLZ]��� ��������[LLZ] �� �����!" // Translate it 2926
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ���������[LLZ]!" // Translate it 2927
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ������� ���." // Translate it 2928
					m_type = 2

		if ("clap")
			if (!src.restrained())
				message = "� <B>[src]</B>  �������." // Translate it 2929
				m_type = 2
				if(miming)
					m_type = 1
		if ("flap")
			if (!src.restrained())
				message = "� <B>[src]</B> ������� ��������." // Translate it 2930
				m_type = 2
				if(miming)
					m_type = 1

		if ("aflap")
			if (!src.restrained())
				message = "� <B>[src]</B> ������ ������� ��������!" // Translate it 2931
				m_type = 2
				if(miming)
					m_type = 1

		if ("drool")
			message = "� <B>[src]</B> ������� �����." // Translate it 2932
			m_type = 1

		if ("eyebrow")
			message = "� <B>[src]</B> ��������� �����." // Translate it 2933
			m_type = 1

		if ("chuckle")
			if(miming)
				message = "� <B>[src]</B> �������� ��������." // Translate it 2934
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ��������." // Translate it 2935
					m_type = 2
				else
					message = "� <B>[src]</B> �����." // Translate it 2936
					m_type = 2

		if ("twitch")
			message = "� <B>[src]</B> [LLZ]������ ��������[LLZ]." // Translate it 2937
			m_type = 1

		if ("twitch_s")
			message = "� <B>[src]</B> ��������[LLZ]." // Translate it 2938
			m_type = 1

		if ("faint")
			message = "� <B>[src]</B> ������ � �������." // Translate it 2939
			if(src.sleeping)
				return //Can't faint while asleep
			src.sleeping += 10 //Short-short nap
			m_type = 1

		if ("cough")
			if(miming)
				message = "� <B>[src]</B> �������� ����[LLZ]��!" // Translate it 2940
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ����[LLZ]��!" // Translate it 2941
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ������� ���." // Translate it 2942
					m_type = 2

		if ("frown")
			message = "� <B>[src]</B> �������[LLZ]." // Translate it 2943
			m_type = 1

		if ("nod")
			message = "� <B>[src]</B> ������." // Translate it 2944
			m_type = 1

		if ("blush")
			message = "� <B>[src]</B> ��������." // Translate it 2945
			m_type = 1

		if ("wave")
			message = "� <B>[src]</B> �������[LLZ]." // Translate it 2946
			m_type = 1

		if ("gasp")
			if(miming)
				message = "� <B>[src]</B>  �� ��������, ���������[LLZ]!" // Translate it 2947
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ���������[LLZ]!" // Translate it 2948
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ������ ���." // Translate it 2949
					m_type = 2

		if ("deathgasp")
			message = "� <B>[src]</B> seizes up and falls limp, \his eyes dead and lifeless..." // Translate it 2950
			m_type = 1

		if ("giggle")
			if(miming)
				message = "� <B>[src]</B> ���� ��������!" // Translate it 2951
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ��������." // Translate it 2952
					m_type = 2
				else
					message = "� <B>[src]</B> �����." // Translate it 2953
					m_type = 2

		if ("glare")
			var/M = null
			if (param)
				for (var/mob/A in view(null, null))
					if (param == A.name)
						M = A
						break
			if (!M)
				param = null

			if (param)
				message = "� <B>[src]</B> [pick("�[LLZ]����[LLZ]","�������")] �� [param]." // Translate it 2954
			else
				message = "� <B>[src]</B> �������." // Translate it 2955

		if ("stare")
			var/M = null
			if (param)
				for (var/mob/A in view(null, null))
					if (param == A.name)
						M = A
						break
			if (!M)
				param = null

			if (param)
				message = "� <B>[src]</B> ��������� �� [param]." // Translate it 2956
			else
				message = "� <B>[src]</B> ���������." // Translate it 2957

		if ("look")
			var/M = null
			if (param)
				for (var/mob/A in view(null, null))
					if (param == A.name)
						M = A
						break

			if (!M)
				param = null

			if (param)
				message = "� <B>[src]</B> ������� �� [param]." // Translate it 2958
			else
				message = "� <B>[src]</B> �������." // Translate it 2959
			m_type = 1

		if ("grin")
			message = "<B>[src]</B> ������ ����." // Translate it 2960
			m_type = 1

		if ("cry")
			if(miming)
				message = "� <B>[src]</B> ������." // Translate it 2961
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> [pick("������","������")]." // Translate it 2962
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ������ ���."// \He frowns. // Translate it 2963
					m_type = 2

		if ("sigh")
			if(miming)
				message = "� <B>[src]</B> ��������." // Translate it 2964
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> �����." // Translate it 2965
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ������ ���." // Translate it 2966
					m_type = 2

		if ("laugh")
			if(miming)
				message = "� <B>[src]</B> ������ ������." // Translate it 2967
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ������[LLZ]." // Translate it 2968
					m_type = 2
				else
					message = "� <B>[src]</B> �����." // Translate it 2969
					m_type = 2

		if ("mumble")
			message = "� <B>[src]</B> ��������!" // Translate it 2970
			m_type = 2
			if(miming)
				m_type = 1

		if ("grumble")
			if(miming)
				message = "� <B>[src]</B> ������!" // Translate it 2971
				m_type = 1
			if (!muzzled)
				message = "� <B>[src]</B> ������!" // Translate it 2972
				m_type = 2
			else
				message = "� <B>[src]</B> �����." // Translate it 2973
				m_type = 2

		if ("groan")
			if(miming)
				message = "� <B>[src]</B> ������ ����!" // Translate it 2974
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> c�����!" // Translate it 2975
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ������� ���." // Translate it 2976
					m_type = 2

		if ("moan")
			if(miming)
				message = "� <B>[src]</B>  �������� �������!" // Translate it 2977
				m_type = 1
			else
				message = "� <B>[src]</B> ������ �� ����������[LLZ]!" // Translate it 2978
				m_type = 2

		if ("johnny")
			var/M
			if (param)
				M = param
			if (!M)
				param = null
			else
				if(miming)
					message = "� <B>[src]</B>  �����������[LLZ] ��������� � �������� ��� \"[M]\" � ����." // Translate it 2979
					m_type = 1
				else
					message = "� <B>[src]</B> says, \"[M], please. He had a family.\" [src.name] �����������[LLZ] ��������� � �������� ���� ��[LLZ]." // Translate it 2980
					m_type = 2

		if ("point")
			if (!src.restrained())
				var/mob/M = null
				if (param)
					for (var/atom/A as mob|obj|turf|area in view(null, null))
						if (param == A.name)
							M = A
							break

				if (!M)
					message = "� <B>[src]</B> ����� �������." // Translate it 2981
				else
					M.point()

				if (M)
					message = "� <B>[src]</B> ���������� �� [M]." // Translate it 2982
				else
			m_type = 1

		if ("raise")
			if (!src.restrained())
				message = "� <B>[src]</B> ��������� ����." // Translate it 2983
			m_type = 1

		if("shake")
			message = "� <B>[src]</B> ��[LLZ]��� �������." // Translate it 2984
			m_type = 1

		if ("shrug")
			message = "� <B>[src]</B> �������� �������." // Translate it 2985
			m_type = 1

		if ("signal")
			if (!src.restrained())
				var/t1 = round(text2num(param))
				if (isnum(t1))
					if (t1 <= 5 && (!src.r_hand || !src.l_hand))
						message = "� <B>[src]</B> ��������� [t1] �������." // Translate it 2986
					else if (t1 <= 10 && (!src.r_hand && !src.l_hand))
						message = "� <B>[src]</B> ��������� [t1] �������." // Translate it 2987
			m_type = 1

		if ("smile")
			message = "� <B>[src]</B> ��������[LLZ]." // Translate it 2988
			m_type = 1

		if ("shiver")
			message = "� <B>[src]</B> �����������." // Translate it 2989
			m_type = 2
			if(miming)
				m_type = 1

		if ("pale")
			message = "� <B>[src]</B> �������� �� �������." // Translate it 2990
			m_type = 1

		if ("tremble")
			message = "� <B>[src]</B> ������ � ������!" // Translate it 2991
			m_type = 1

		if ("sneeze")
			if (miming)
				message = "<B>[src]</B> ������." // Translate it 2992
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ������." // Translate it 2993
					m_type = 2
				else
					message = "� <B>[src]</B> ������ �������� ���." // Translate it 2994
					m_type = 2

		if ("sniff")
			message = "� <B>[src]</B> ������." // Translate it 2995
			m_type = 2
			if(miming)
				m_type = 1

		if ("snore")
			if (miming)
				message = "� <B>[src]</B> ������ �����." // Translate it 2996
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ������." // Translate it 2997
					m_type = 2
				else
					message = "� <B>[src]</B> �����." // Translate it 2998
					m_type = 2

		if ("whimper")
			if (miming)
				message = "� <B>[src]</B> ������ �� ����." // Translate it 2999
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ������." // Translate it 3000
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ������ ���." // Translate it 3001
					m_type = 2

		if ("wink")
			message = "� <B>[src]</B> �����������." // Translate it 3002
			m_type = 1

		if ("yawn")
			if (!muzzled)
				message = "� <B>[src]</B> ������." // Translate it 3003
				m_type = 2
				if(miming)
					m_type = 1

		if ("collapse")
			Paralyse(2)
			message = "� <B>[src]</B> ������!" // Translate it 3004
			m_type = 2
			if(miming)
				m_type = 1

		if("hug")
			m_type = 1
			if (!src.restrained())
				var/M = null
				if (param)
					for (var/mob/A in view(1, null))
						if (param == A.name)
							M = A
							break
				if (M == src)
					M = null

				if (M)
					message = "� <B>[src]</B> �������� [M]." // Translate it 3005
				else
					message = "� <B>[src]</B> �������� ���[LLZ]." // Translate it 3006

		if ("handshake")
			m_type = 1
			if (!src.restrained() && !src.r_hand)
				var/mob/M = null
				if (param)
					for (var/mob/A in view(1, null))
						if (param == A.name)
							M = A
							break
				if (M == src)
					M = null

				if (M)
					if (M.canmove && !M.r_hand && !M.restrained())
						message = "� <B>[src]</B> �������� ���� [M]." // Translate it 3007
					else
						message = "� <B>[src]</B> ����[LLZ]������ ���� [M]." // Translate it 3008

		if("dap")
			m_type = 1
			if (!src.restrained())
				var/M = null
				if (param)
					for (var/mob/A in view(1, null))
						if (param == A.name)
							M = A
							break
				if (M)
					message = "� <B>[src]</B> ��� �������� [M]." // Translate it 3009
				else
					message = "� <B>[src]</B> ��������������� ��� �� ����� ����� ���� ���� �������� ��� �������� ����. ��������." // Translate it 3010

		if ("scream")
			if (miming)
				message = "� <B>[src]</B> ������!" // Translate it 3011
				m_type = 1
			else
				if (!muzzled)
					message = "� <B>[src]</B> ������!" // Translate it 3012
					call_sound_emote("scream")
					m_type = 2
				else
					message = "� <B>[src]</B> ������ ����� ������� ���." // Translate it 3013
					m_type = 2

		if ("help")
			src << "blink, blink_r, blush, bow-(none)/mob, burp, choke, chuckle, clap, collapse, cough,\ncry, custom, deathgasp, drool, eyebrow, frown, gasp, giggle, groan, grumble, handshake, hug-(none)/mob, glare-(none)/mob,\ngrin, laugh, look-(none)/mob, moan, mumble, nod, pale, point-atom, raise, salute, shake, shiver, shrug,\nsigh, signal-#1-10, smile, sneeze, sniff, snore, stare-(none)/mob, tremble, twitch, twitch_s, whimper,\nwink, yawn"

		else
			message = "� <B>[src]</B> [act]."




	if (message)
		log_emote("[name]/[key] : [message]")

 //Hearing gasp and such every five seconds is not good emotes were not global for a reason.
 // Maybe some people are okay with that.

		for(var/mob/M in dead_mob_list)
			if(!M.client || istype(M, /mob/new_player))
				continue //skip monkeys, leavers and new players
			if(M.stat == DEAD && (M.client.prefs.toggles & CHAT_GHOSTSIGHT) && !(M in viewers(src,null)))
				M.show_message(message)


		if (m_type & 1)
			for (var/mob/O in viewers(src, null))
				O.show_message(message, m_type)
		else if (m_type & 2)
			for (var/mob/O in hearers(src.loc, null))
				O.show_message(message, m_type)

/mob/living/carbon/human/verb/pose()
	set name = "Set Pose"
	set desc = "Sets a description which will be shown when someone examines you."
	set category = "IC"

	pose =  copytext(sanitize_uni(input(usr, "This is [src]. \He is...", "Pose", null)  as text), 1, MAX_MESSAGE_LEN)


/mob/living/carbon/human/verb/set_flavor()
	set name = "Set Flavour Text"
	set desc = "Sets an extended description of your character's features."
	set category = "IC"

	flavor_text =  copytext(sanitize(input(usr, "Please enter your new flavour text.", "Flavour text", null)  as text), 1)

/mob/living/carbon/human/proc/call_sound_emote(var/E)
	switch(E)
		if("scream")
			for(var/mob/M in viewers(usr, null))
				if (src.gender == "male")
					M << sound(pick('sound/voice/human_emo/Screams_Male_1.ogg', 'sound/voice/human_emo/Screams_Male_2.ogg', 'sound/voice/human_emo/Screams_Male_3.ogg'))
				else
					M << sound(pick('sound/voice/human_emo/Screams_Woman_1.ogg', 'sound/voice/human_emo/Screams_Woman_2.ogg', 'sound/voice/human_emo/Screams_Woman_3.ogg'))
