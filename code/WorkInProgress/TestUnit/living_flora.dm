/obj/item/herb
	name = "Herb"
	icon = 'icons/obj/items.dmi'
	icon_state = "wrap_paper"
	var/amount = 1
	var/MAX_AMOUNT = 5

/obj/item/herb/New()
	..()
	pixel_x = rand(-7,7)
	pixel_y = rand(-7,7)

/obj/item/stick
	name = "Stick"
	icon = 'icons/obj/items.dmi'
	icon_state = "deliveryPaper"
	var/amount = 1
	var/MAX_AMOUNT = 5

/obj/item/stick/New()
	..()
	pixel_x = rand(-7,7)
	pixel_y = rand(-7,7)

/obj/item/wood
	name = "Wood"
	icon = 'icons/obj/resources.dmi'
	icon_state = "wood"
	var/amount = 1
	var/MAX_AMOUNT = 5

/obj/item/wood/New()
	..()
	icon_state = "wood_[amount]"
	pixel_x = rand(-7,7)
	pixel_y = rand(-7,7)

/obj/item/wood/attackby(obj/item/W as obj, mob/user as mob)
	if (!W)								return
	if (!istype(W, /obj/item/wood))		return
	if (src:amount<0)					return
	if (W:amount>=W:MAX_AMOUNT)
		user << "<font color=red>�� ���� ��[LLZ]�� ������.</font>"
		return

	W:amount = W:amount + src:amount
	if(W:amount>W:MAX_AMOUNT)
		var/excess = W:amount - W:MAX_AMOUNT
		src:amount = excess
		src.icon_state = "wood_[excess]"
		W:amount = W:MAX_AMOUNT
		W.icon_state = "wood_[W:amount]"
	visible_message("<font color=purple><b>[user]</b> ���������� [src] � ����.</font>")
	W.icon_state = "wood_[W:amount]"
	del(src)
	return