/obj/machinery/computer/musicarcade
	name = "music arcade machine"
	desc = "Stamp on the ground."
	icon = 'icons/obj/computer.dmi'
	icon_state = "arcade1"
	var/obj/machinery/status_display/mconnected = null
	var/obj/machinery/step_tile/sconnected = null
	var/mobstepmove = "North"
	var/score

/obj/machinery/computer/musicarcade/proc/mUpdate(var/mobdir)
	if(mobdir == mobstepmove)
		score += 5
	else
		score -=10

	var/direction
	var/dirtext
	switch(pick(1,2,4,8))
		if(1)
			direction = NORTH
			dirtext = "North"
		if(2)
			direction = SOUTH
			dirtext = "South"
		if(4)
			direction = WEST
			dirtext = "West"
		if(8)
			direction = EAST
			dirtext = "East"
	mobstepmove = direction
	mconnected.update_display(score,dirtext)

/obj/machinery/computer/musicarcade/process()
	if(stat & (NOPOWER|BROKEN))
//		world << "[src] process off."
		return
	src.updateUsrDialog()
//	world << "[src] process on."
	return

/obj/machinery/computer/musicarcade/New()
	..()
	spawn(5)
		var/obj/machinery/status_display/M = new /obj/machinery/status_display(src.loc)
		var/obj/machinery/step_tile/S = new /obj/machinery/step_tile(get_step(src, SOUTH))
		src.sconnected = S
		src.mconnected = M
		S.mconnected = src
		mconnected.mode = 5
		return
	return

/obj/machinery/computer/musicarcade/ex_act(severity)
	switch(severity)
		if(1.0)
			//SN src = null
			del(src)
			del(mconnected)
			del(sconnected)
			return
		if(2.0)
			if (prob(50))
				//SN src = null
				del(src)
				del(mconnected)
				del(sconnected)
				return
		else
	return






/obj/machinery/step_tile
	name = "Step tile"
	desc = "Jump jump jump jump..."
	icon = 'icons/turf/floors.dmi'
	icon_state = "gcircuitoff"
	var/obj/machinery/computer/musicarcade/mconnected
	var/mob/living/buckled_mob = null
/*
/obj/machinery/step_tile/New()
	..()
	spawn(5)
		var/obj/machinery/computer/musicarcade/mconnected = locate(/obj/machinery/computer/musicarcade, get_step(src, NORTH))
		return
	return
*/
/obj/machinery/step_tile/process()
	if(stat & (NOPOWER|BROKEN))
		icon_state = "gcircuitoff"
//		world << "[src] process off."
		return
	icon_state = "gcircuit"
//	world << "[src] process on."
	return

/obj/machinery/step_tile/proc/sUpdate(var/mob/M, var/moveDir)
	mconnected.mUpdate(moveDir)
	return

/obj/machinery/step_tile/attack_hand(mob/user as mob)
	manual_unbuckle(user)
	return

/obj/machinery/step_tile/MouseDrop(atom/over_object)
	return

/obj/machinery/step_tile/MouseDrop_T(mob/M as mob, mob/user as mob)
	if(!istype(M)) return
	buckle_mob(M, user)
	return

/obj/machinery/step_tile/proc/unbuckle()
	if(buckled_mob)
		if(buckled_mob.buckled == src)	//this is probably unneccesary, but it doesn't hurt
			buckled_mob.buckled = null
			buckled_mob.anchored = initial(buckled_mob.anchored)
			buckled_mob.update_canmove()
			buckled_mob = null
	return

/obj/machinery/step_tile/proc/manual_unbuckle(mob/user as mob)
	if(buckled_mob)
		if(buckled_mob.buckled == src)
			if(buckled_mob != user)
				buckled_mob.visible_message(\
					"\blue [buckled_mob.name] was unbuckled by [user.name]!",\
					"You were unbuckled from [src] by [user.name].",\
					"You hear metal clanking")
			else
				buckled_mob.visible_message(\
					"\blue [buckled_mob.name] unbuckled \himself!",\
					"You unbuckle yourself from [src].",\
					"You hear metal clanking")
			unbuckle()
			src.add_fingerprint(user)
	return

/obj/machinery/step_tile/proc/buckle_mob(mob/M as mob, mob/user as mob)
	if (!ticker)
		user << "You can't buckle anyone in before the game starts."
	if ( !ismob(M) || (get_dist(src, user) > 1) || (M.loc != src.loc) || user.restrained() || user.lying || user.stat || M.buckled || istype(user, /mob/living/silicon/pai) )
		return

	if (istype(M, /mob/living/carbon/slime))
		user << "The [M] is too squishy to buckle in."
		return

	unbuckle()

	if (M == usr)
		M.visible_message(\
			"\blue [M.name] buckles in!",\
			"You buckle yourself to [src].",\
			"You hear metal clanking")
	else
		M.visible_message(\
			"\blue [M.name] is buckled in to [src] by [user.name]!",\
			"You are buckled in to [src] by [user.name].",\
			"You hear metal clanking")
	M.buckled = src
	M.loc = src.loc
	M.update_canmove()
	src.buckled_mob = M
	src.add_fingerprint(user)
	return